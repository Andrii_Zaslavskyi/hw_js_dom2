
document.addEventListener("DOMContentLoaded", function() {
    const chooseQuantity = document.getElementById('limitSelect');
    const userList = document.querySelector('.list');

    function loadUsers(quantity) {
        fetch(`https://jsonplaceholder.typicode.com/users?_page=1&_limit=${quantity}`)
        .then((response) => response.json())
        .then((data) => {
            userList.innerHTML = '';

            data.forEach(user => {
                const li = document.createElement('li');
//     if (user.name.includes('Mrs.')) {
//         firstName = user.name.slice(4, user.name.indexOf(' '));
//     } else {
//     firstName = user.name.slice(0, user.name.indexOf(' '))
// };
                let firstName = user.name.slice(0, user.name.indexOf(' '));
                let secondName = user.name.slice(user.name.indexOf(' '));
                li.innerHTML = `Ім'я: ${firstName}, Прізвище: ${secondName}, Пошта: ${user.email}, Сайт: ${user.website}`;
                if (user.id % 2 === 0) {
                    li.style.backgroundColor = 'gray';
                }
                userList.append(li);
            });
        });
    };

    loadUsers(1);
    
    chooseQuantity.addEventListener('change', function() {
        const selectedQuantity = this.value;
        loadUsers(selectedQuantity); 
    });
});